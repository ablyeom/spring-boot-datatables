package org.springframework.data.chexcar.datatables;

import com.querydsl.core.types.dsl.PathBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;


/**
 * @author abel
 */
interface Filter {

    Predicate createPredicate(From<?, ?> from, CriteriaBuilder criteriaBuilder, String attributeName);

    com.querydsl.core.types.Predicate createPredicate(PathBuilder<?> pathBuilder, String attributeName);
}